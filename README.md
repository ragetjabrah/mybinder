# mybinder
754 GB RAM & 72 CPU's👉 [![Binder](https://mybinder.org/badge_logo.svg)](https://notebooks.gesis.org/v2/git/https://gitlab.com/ragetjabrah/mybinder/main)

51 GB RAM & 8 CPU's👉 [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https://gitlab.com/ragetjabrah/mybinder/main)

* _Tutorial details go👉 https://aank.me/Youtube_

----------------------
# GColab
SSH with 35 GB RAM & 40 CPU's👉 [![colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/a2nk/notebooks/blob/main/SSH_with_40CPU.ipynb)

* _Tutorial details go👉 https://aank.me/Youtube_
